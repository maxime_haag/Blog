<?php
    require('controller/controller.php');
    require('controller/controller_liens.php');
    require('controller/controller_articles.php');

   if (isset($_GET['action'])) 
    {
        if ($_GET['action'] == 'sources') 
        {
            print_sources();
        }
        
        elseif ($_GET['action'] == 'article') 
        {
            if (isset($_GET['articles'])  /*/&& $_GET['articles'] =< $nbr_art/*/) 
            {
                $check = check_pub($_GET['articles']);
                
                if( $check != '0')
                {
                print_art($_GET['articles']);
                }
                
                else
                {
                    require('function/404.php');
                }
            }

            else
            {
                require('function/404.php');
            }
            
        }
        
        elseif ($_GET['action'] == 'contact')
        {   
            if(isset($_GET['check']) && $_GET['check'] == 'envoyer')
            {
                contact_send($_POST['email'],$_POST['nom'],$_POST['message']);
            
            }
            else
            {
                contact();
            }
        }

        elseif ($_GET['action'] == 'lien')
        {
            print_link();
        }
        
        elseif ($_GET['action'] == 'news')
        {
            ob_start();
            require('view/news.php');
            $content = ob_get_clean(); 
            require('view/templates.php');
        }
 
        elseif ($_GET['action'] == 'construct')
        {
            require('view/under_construct.php');
        }
    }
    
    else 
    {
         accueil();
    }