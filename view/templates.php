<!DOCTYPE html>
<html>
<?php require('view/head.php'); ?>     
    <body>
        <?php require('view/header.php'); ?>
        <div class='main'>
          <?= $content ?>
        </div>
        <?php require('view/foot.php'); ?>
    </body>
</html>
